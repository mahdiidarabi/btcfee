package btcfee

const (
	P2PKH_IN_SIZE        int64 = 148
	P2PKH_OUT_SIZE       int64 = 34
	P2SH_OUT_SIZE        int64 = 32
	P2SH_P2WPKH_OUT_SIZE int64 = 32
	P2SH_P2WSH_OUT_SIZE  int64 = 32
	P2SH_P2WPKH_IN_SIZE  int64 = 91
	P2WPKH_IN_SIZE       int64 = 68
	P2WPKH_OUT_SIZE      int64 = 31
	P2WSH_OUT_SIZE       int64 = 43
	P2TR_OUT_SIZE        int64 = 43
	P2TR_IN_SIZE         int64 = 58
	PUBKEY_SIZE          int64 = 33
	SIGNATURE_SIZE       int64 = 72

	P2PKH       = "p2pkh"
	P2SH        = "p2sh"
	P2SH_P2WPKH = "p2sh(p2wpkh)"
	P2SH_P2WSH  = "p2sh(p2wsh)"
	P2WPKH      = "p2wpkh"
	P2WSH       = "p2wsh"
	P2TR        = "p2tr"
)
