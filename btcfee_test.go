package btcfee

import "testing"

func TestTxSizeCalc(t *testing.T) {
	// from https://github.com/jlopp/bitcoin-transaction-size-calculator
	// https://jlopp.github.io/bitcoin-transaction-size-calculator/

	var input_num int64 = 15
	input_script := P2SH_P2WPKH

	output_script := []string{P2PKH, P2SH, P2SH_P2WPKH, P2SH_P2WSH, P2WPKH, P2WSH }
	output_num := []int64{1, 1, 1, 1, 1, 1}

	var sig_per_input int64 = 1
	var pub_per_input int64 = 1

	var txByteFromSite int64 = 3187

	txByteSize, err := TxSizeCalc(input_script, input_num, sig_per_input, pub_per_input, output_script, output_num)
	if err != nil {
		t.Errorf("got error")
	}

	if (txByteSize - txByteFromSite) > txByteFromSite/100 || (txByteFromSite - txByteSize) > txByteFromSite/100 {
		t.Errorf("got more than 1 percent difference")
	}


	input_num = 14
	input_script = P2WPKH

	output_script = []string{P2PKH, P2SH, P2SH_P2WPKH, P2SH_P2WSH, P2WPKH, P2WSH }
	output_num = []int64{6, 5, 4, 3, 2, 1}

	sig_per_input = 1
	pub_per_input = 1

	txByteFromSite = 3153

	txByteSize, err = TxSizeCalc(input_script, input_num, sig_per_input, pub_per_input, output_script, output_num)
	if err != nil {
		t.Errorf("got error")
	}

	if (txByteSize - txByteFromSite) > txByteFromSite/100 || (txByteFromSite - txByteSize) > txByteFromSite/100 {
		t.Errorf("got more than 1 percent difference")
	}

	input_num = 9
	input_script = P2PKH

	output_script = []string{P2PKH, P2SH, P2SH_P2WPKH, P2SH_P2WSH, P2WPKH, P2WSH }
	output_num = []int64{1, 2, 3, 4, 5, 6}

	sig_per_input = 1
	pub_per_input = 1

	txByteFromSite = 2077

	txByteSize, err = TxSizeCalc(input_script, input_num, sig_per_input, pub_per_input, output_script, output_num)
	if err != nil {
		t.Errorf("got error")
	}

	if (txByteSize - txByteFromSite) > txByteFromSite/100 || (txByteFromSite - txByteSize) > txByteFromSite/100 {
		t.Errorf("got more than 1 percent difference")
	}
}
