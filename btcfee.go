package btcfee

import (
	"fmt"
)

func TxSizeCalc(inputScript string, inputCount int64, sigPerInput int64, pubPerInput int64, outputScript []string, outputCount []int64) (int64, error) {

	if len(outputCount) != len(outputScript) {
		return 0, fmt.Errorf("length of output_script and output_count should be same")
	}

	inputSize, inputWitnessSize := calcInputPayLoad(inputScript, inputCount, sigPerInput, pubPerInput)

	txVBytes := calcOutputPayLoad(inputScript, inputCount, outputScript, outputCount)

	txBytes := getTxOverheadExtraRawBytes(inputScript, inputCount) + txVBytes + inputSize*inputCount + inputWitnessSize*inputCount
	return txBytes, nil
}

func calcOutputPayLoad(inputScript string, inputCount int64, outputScript []string, outputCount []int64) int64 {
	var (
		totalOutputCount      int64 = 0
		p2pkhOutputCount      int64 = 0
		p2shOutputCount       int64 = 0
		p2shP2wpkhOutputCount int64 = 0
		p2shP2wshOutputCount  int64 = 0
		p2wpkhOutputCount     int64 = 0
		p2wshOutputCount      int64 = 0
		p2trOutputCount       int64 = 0
	)

	for i := 0; i < len(outputScript); i++ {

		totalOutputCount += outputCount[i]

		switch outputScript[i] {
		case P2PKH:
			p2pkhOutputCount = outputCount[i]

		case P2SH:
			p2shOutputCount = outputCount[i]

		case P2SH_P2WPKH:
			p2shP2wpkhOutputCount = outputCount[i]

		case P2SH_P2WSH:
			p2shP2wshOutputCount = outputCount[i]

		case P2WPKH:
			p2wpkhOutputCount = outputCount[i]

		case P2WSH:
			p2wshOutputCount = outputCount[i]

		case P2TR:
			p2trOutputCount = outputCount[i]

		default:

		}
	}

	var txVBytes = getTxOverheadVBytes(inputScript, inputCount, totalOutputCount) +
		P2PKH_OUT_SIZE*p2pkhOutputCount +
		P2SH_OUT_SIZE*p2shOutputCount +
		P2SH_P2WPKH_OUT_SIZE*p2shP2wpkhOutputCount +
		P2SH_P2WSH_OUT_SIZE*p2shP2wshOutputCount +
		P2WPKH_OUT_SIZE*p2wpkhOutputCount +
		P2WSH_OUT_SIZE*p2wshOutputCount +
		P2TR_OUT_SIZE*p2trOutputCount

	return txVBytes
}

func calcInputPayLoad(inputScript string, inputCount int64, sigPerInput int64, pubPerInput int64) (int64, int64) {

	var (
		inputSize        int64 = 0
		inputWitnessSize int64 = 0
	)

	switch inputScript {
	case P2PKH:
		inputSize = P2PKH_IN_SIZE

	case P2SH_P2WPKH:
		inputSize = P2SH_P2WPKH_IN_SIZE
		// size(signature) + signature + size(pubkey) + pubkey
		inputWitnessSize = 107

	case P2WPKH:
		inputSize = P2WPKH_IN_SIZE
		// size(signature) + signature + size(pubkey) + pubkey
		inputWitnessSize = 107
	case P2TR:
		inputSize = P2TR_IN_SIZE
		// getSizeOfVarInt(schnorrSignature) + schnorrSignature
		inputWitnessSize = 65

	case P2SH:
		var redeemScriptSize = 1 + pubPerInput*(1+PUBKEY_SIZE) + 1 + 1
		var scriptSigSize = 1 + sigPerInput*(1+SIGNATURE_SIZE) + getSizeOfScriptLengthElement(redeemScriptSize) + redeemScriptSize
		inputSize = 32 + 4 + getSizeOfVarInt(scriptSigSize) + scriptSigSize + 4

	case P2SH_P2WSH:
		var redeemScriptSize = 1 + pubPerInput*(1+PUBKEY_SIZE) + 1 + 1
		inputWitnessSize = 1 + sigPerInput*(1+SIGNATURE_SIZE) + getSizeOfScriptLengthElement(redeemScriptSize) + redeemScriptSize
		inputSize = 36 + inputWitnessSize/4 + 4
		if inputScript == "P2SH-P2WSH" {
			// P2SH wrapper (redeemscript hash) + overhead?
			inputSize += 32 + 3
		}

	default:
		return 0, 0
	}

	return inputSize, inputWitnessSize
}

func getTxOverheadExtraRawBytes(inputScript string, inputCount int64) int64 {
	var witnessVbytes int64 = 0
	if inputScript == "P2PKH" || inputScript == "P2SH" {
		witnessVbytes = 0
	} else {
		// Transactions with segwit inputs have extra overhead
		// segwit market, segwit flag, segwit element count
		witnessVbytes = 1 + getSizeOfVarInt(inputCount)/4
	}

	return witnessVbytes * 3
}

func getTxOverheadVBytes(inputScript string, inputCount int64, outputCount int64) int64 {
	var witnessVbytes int64 = 0

	if inputScript == "P2PKH" || inputScript == "P2SH" {
		witnessVbytes = 0
	} else {
		// Transactions with segwit inputs have extra overhead
		// segwit marker, segwit flag, witness element count
		witnessVbytes = 1 + getSizeOfVarInt(inputCount)/4
	}

	// nVersion, number of inputs, number of outputs, nLockTime
	return 4 + getSizeOfVarInt(inputCount) + getSizeOfVarInt(outputCount) + 4 + witnessVbytes
}

func getSizeOfVarInt(length int64) int64 {
	if length < 253 {
		return 1
	} else if length < 65535 {
		return 3
	} else if length < 4294967295 {
		return 5
	} else {
		return 9
	}
}

func getSizeOfScriptLengthElement(length int64) int64 {
	if length < 75 {
		return 1
	} else if length <= 255 {
		return 2
	} else if length <= 65535 {
		return 3
	} else if length <= 4294967295 {
		return 5
	} else {
		// if the execution reach to this point, probably you generate
		// a very large redeem script and perhaps not accepted by miners
		return 7
	}
}
